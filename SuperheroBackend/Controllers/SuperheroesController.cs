﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SuperheroBackend.Models;

namespace SuperheroBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuperheroesController : ControllerBase
    {
        private readonly SuperheroManager _superheroManager;

        public SuperheroesController(SuperheroManager superheroManager)
        {
            _superheroManager = superheroManager;
        }

        // GET: api/Superheros

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Superhero>>> GetSuperheros()
        {
            var superheros = await _superheroManager.GetAllSuperherosAsync();
            return Ok(superheros);
        }

        [Route("Superheroes3")]
        [HttpGet]
        //GET: api/Last3Superheros
        public async Task<ActionResult<IEnumerable<Superhero>>> GetLast3Superheros()
        {
            var superheros = await _superheroManager.GetLast3SuperherosAsync();
            return Ok(superheros);
        }

        // GET: api/Superheros/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Superhero>> GetSuperhero(int id)
        {
            var superhero = await _superheroManager.GetSuperheroByIdAsync(id);
            if (superhero == null)
            {
                return NotFound();
            }

            return Ok(superhero);
        }

        [Route("{powerId}/powerId")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Superhero>>> GetSuperherosByOwner(int powerId)
        {
            var superheros = await _superheroManager.GetSuperheroByPowerAsync(powerId);
            if (superheros == null || !superheros.Any())
            {
                return NotFound();
            }

            return Ok(superheros);
        }

        // PUT: api/Superheros/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSuperhero(int id, Superhero superhero)
        {
            if (id != superhero.Id)
            {
                return BadRequest();
            }

            var updatedSuperhero = await _superheroManager.UpdateSuperheroAsync(id, superhero);
            if (updatedSuperhero == null)
            {
                return NotFound();
            }
            return NoContent();
        }
        // POST: api/Superheros
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Superhero>> PostSuperhero(Superhero superhero)
        {
            await _superheroManager.AddSuperheroAsync(superhero);

            return CreatedAtAction("GetSuperhero", new { id = superhero.Id }, superhero);
        }

        // DELETE: api/Superheros/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSuperhero(int id)
        {
            var deletedSuperhero = await _superheroManager.DeleteSuperheroAsync(id);
            if (deletedSuperhero == null)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
