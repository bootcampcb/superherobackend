﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SuperheroBackend.Models;

namespace SuperheroBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuperpowersController : ControllerBase
    {
        private readonly SuperpowersManager _manager;

        public SuperpowersController(SuperpowersManager manager)
        {
            _manager = manager;
        }

        // GET: api/Superpowers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Superpower>>> GetSuperpowers()
        {
            var superpowers = await _manager.GetAllSuperpowersAsync();
            return Ok(superpowers);
        }

        // GET: api/Superpowers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Superpower>> GetSuperpower(int id)
        {
            var superpower = await _manager.GetSuperpowerByIdAsync(id);

            if (superpower == null)
            {
                return NotFound();
            }

            return Ok(superpower);
        }

        // PUT: api/Superpowers/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSuperpower(int id, Superpower superpower)
        {
            if (id != superpower.Id)
            {
                return BadRequest();
            }

            var success = await _manager.UpdateSuperpowerAsync(id, superpower);

            if (!success)
            {
                return NotFound();
            }

            return NoContent();
        }

        // POST: api/Superpowers
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Superpower>> PostSuperpower(Superpower superpower)
        {
            var createdSuperpower = await _manager.AddSuperpowerAsync(superpower);

            return CreatedAtAction("GetSuperpower", new { id = createdSuperpower.Id }, createdSuperpower);
        }

        // DELETE: api/Superpowers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSuperpower(int id)
        {
            var success = await _manager.DeleteSuperpowerAsync(id);

            if (!success)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
