﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace SuperheroBackend.Migrations
{
    /// <inheritdoc />
    public partial class InitialDbBuildAndSeed : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Superpowers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Superpowers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Superheroes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Nickname = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TelephoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DateOfBirth = table.Column<DateTime>(type: "datetime2", nullable: false),
                    SuperpowerId = table.Column<int>(type: "int", nullable: true),
                    WeaknessId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Superheroes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Superheroes_Categories_WeaknessId",
                        column: x => x.WeaknessId,
                        principalTable: "Categories",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "SuperpowerCategory",
                columns: table => new
                {
                    SuperpowerId = table.Column<int>(type: "int", nullable: false),
                    CategoryId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuperpowerCategory", x => new { x.SuperpowerId, x.CategoryId });
                    table.ForeignKey(
                        name: "FK_SuperpowerCategory_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SuperpowerCategory_Superpowers_SuperpowerId",
                        column: x => x.SuperpowerId,
                        principalTable: "Superpowers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SuperheroSuperpower",
                columns: table => new
                {
                    SuperheroId = table.Column<int>(type: "int", nullable: false),
                    SuperpowerId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuperheroSuperpower", x => new { x.SuperheroId, x.SuperpowerId });
                    table.ForeignKey(
                        name: "FK_SuperheroSuperpower_Superheroes_SuperheroId",
                        column: x => x.SuperheroId,
                        principalTable: "Superheroes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SuperheroSuperpower_Superpowers_SuperpowerId",
                        column: x => x.SuperpowerId,
                        principalTable: "Superpowers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Superheroes with fire-based abilities", "Fire" },
                    { 2, "Superheroes with ice-based abilities", "Ice" },
                    { 3, "Superheroes with technology-based abilities", "Technology" },
                    { 4, "Superheroes with elemental abilities", "Elemental" },
                    { 5, "Superheroes with mental abilities", "Mental" }
                });

            migrationBuilder.InsertData(
                table: "Superpowers",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Ability to lift extremely heavy objects", "Super Strength" },
                    { 2, "Ability to fly through the air", "Flight" },
                    { 3, "Ability to become invisible", "Invisibility" },
                    { 4, "Ability to move at incredible speeds", "Super Speed" },
                    { 5, "Ability to instantly move from one location to another", "Teleportation" },
                    { 6, "Ability to move objects with the power of the mind", "Telekinesis" },
                    { 7, "Ability to transform one's body into any form", "Shape-Shifting" },
                    { 8, "Ability to heal oneself at an accelerated rate", "Regeneration" },
                    { 9, "Ability to stretch one's body to great lengths", "Elasticity" },
                    { 10, "Ability to control the weather", "Weather Manipulation" },
                    { 11, "Ability to shoot blasts of energy from one's hands or eyes", "Energy Blasts" },
                    { 12, "Ability to create protective force fields", "Force Fields" },
                    { 13, "Ability to mimic the abilities of animals", "Animal Mimicry" },
                    { 14, "Ability to manipulate time", "Time Manipulation" },
                    { 15, "Ability to manipulate gravity", "Gravity Manipulation" }
                });

            migrationBuilder.InsertData(
                table: "Superheroes",
                columns: new[] { "Id", "DateOfBirth", "Name", "Nickname", "SuperpowerId", "TelephoneNumber", "WeaknessId" },
                values: new object[,]
                {
                    { 1, new DateTime(1978, 12, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "Superman", "Man of Steel", 1, "(313) 555-1234", 1 },
                    { 2, new DateTime(1962, 8, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "Spider-Man", "Web-Slinger", 4, "(313) 555-2345", 2 },
                    { 3, new DateTime(1941, 10, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "Wonder Woman", "Amazon Princess", 2, "(313) 555-3456", 1 },
                    { 4, new DateTime(1940, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Flash", "Scarlet Speedster", 4, "(313) 555-4567", 3 },
                    { 5, new DateTime(1939, 5, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Batman", "Dark Knight", null, "(313) 555-5678", 4 },
                    { 6, new DateTime(1963, 3, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Iron Man", "Armored Avenger", 3, "(313) 555-6789", 3 },
                    { 7, new DateTime(1941, 3, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Captain America", "Sentinel of Liberty", 5, "(313) 555-7890", 5 },
                    { 8, new DateTime(1941, 11, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Aquaman", "King of the Seven Seas", 2, "(313) 555-1111", 1 },
                    { 9, new DateTime(1940, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Green Lantern", "Emerald Knight", 6, "(313) 555-333", 4 },
                    { 10, new DateTime(1962, 8, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Thor", "God of Thunder", 7, "(313) 555-5555", 5 }
                });

            migrationBuilder.InsertData(
                table: "SuperpowerCategory",
                columns: new[] { "CategoryId", "SuperpowerId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 2, 2 },
                    { 3, 2 }
                });

            migrationBuilder.InsertData(
                table: "SuperheroSuperpower",
                columns: new[] { "SuperheroId", "SuperpowerId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 2 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Superheroes_WeaknessId",
                table: "Superheroes",
                column: "WeaknessId");

            migrationBuilder.CreateIndex(
                name: "IX_SuperheroSuperpower_SuperpowerId",
                table: "SuperheroSuperpower",
                column: "SuperpowerId");

            migrationBuilder.CreateIndex(
                name: "IX_SuperpowerCategory_CategoryId",
                table: "SuperpowerCategory",
                column: "CategoryId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SuperheroSuperpower");

            migrationBuilder.DropTable(
                name: "SuperpowerCategory");

            migrationBuilder.DropTable(
                name: "Superheroes");

            migrationBuilder.DropTable(
                name: "Superpowers");

            migrationBuilder.DropTable(
                name: "Categories");
        }
    }
}
