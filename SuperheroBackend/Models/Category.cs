﻿using System;
using System.Collections.Generic;

namespace SuperheroBackend.Models;

public partial class Category
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string Description { get; set; } = null!;

    public virtual ICollection<Superhero> Superheroes { get; set; } = new List<Superhero>();

    public virtual ICollection<Superpower> Superpowers { get; set; } = new List<Superpower>();
}
