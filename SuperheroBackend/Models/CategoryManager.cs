﻿using Microsoft.EntityFrameworkCore;
using SuperheroBackend.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuperheroBackend.Models
{
    public class CategoryManager
    {
        private readonly SuperherosDbContext _context;

        public CategoryManager(SuperherosDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Category>> GetAllCategories()
        {
            return await _context.Categories
               .Include(s => s.Superpowers)
               .ToListAsync();
//            return await _context.Categories.ToListAsync();
        }

        public async Task<Category> GetCategoryById(int id)
        {
            var categories = await _context.Categories
                .Include(s => s.Superpowers)
                .ThenInclude(sp => sp.Superheroes)
                .Where(s => s.Id == id)
                .ToListAsync();
            return categories[0];
            //            return await _context.Categories.FindAsync(id);
        }

        public async Task<bool> AddCategory(Category category)
        {
            if (_context.Categories == null)
            {
                return false;
            }
            for (int i = 0; i < category.Superpowers.Count; i++)
            {
                _context.Entry(category.Superpowers.ToList()[i]).State = EntityState.Unchanged;
            }

            _context.Categories.Add(category);
            await _context.SaveChangesAsync();

            return true;
            //_context.Categories.Add(category);
            //return await _context.SaveChangesAsync() > 0;
        }

        public async Task<bool> UpdateCategory(int id, Category category)
        {

            if (id != category.Id)
            {
                return false;
            }

            Category oldCategory = _context.Categories.Include(s => s.Superpowers).Where(s => s.Id == id).ToList()[0];

            _context.Entry(oldCategory).CurrentValues.SetValues(category);

            // Remove unchecked superpowers
            foreach (var superpower in oldCategory.Superpowers.ToList())
            {
                if (!category.Superpowers.Any(sp => sp.Id == superpower.Id))
                {
                    oldCategory.Superpowers.Remove(superpower);
                }
            }

            foreach (var superpower in category.Superpowers)
            {
                var existingSuperpower = oldCategory.Superpowers
                    .FirstOrDefault(sp => sp.Id == superpower.Id);

                if (existingSuperpower == null)
                {
                    oldCategory.Superpowers.Add(superpower);
                }
                else
                {
                    _context.Entry(existingSuperpower).CurrentValues.SetValues(superpower);
                }
            }

            //_context.Entry(category).State = EntityState.Modified;

            try
            {
                return await _context.SaveChangesAsync() > 0;
            }
            catch (DbUpdateConcurrencyException)
            {
                return CategoryExists(id);
            }
        }

        public async Task<bool> DeleteCategory(int id)
        {
            var category = await _context.Categories.FindAsync(id);
            if (category == null)
            {
                return false;
            }

            _context.Categories.Remove(category);
            return await _context.SaveChangesAsync() > 0;
        }

        public bool CategoryExists(int id)
        {
            return _context.Categories.Any(e => e.Id == id);
        }
    }
}
