﻿using System;
using System.Collections.Generic;

namespace SuperheroBackend.Models;

public partial class Superhero
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string Nickname { get; set; } = null!;

    public string TelephoneNumber { get; set; } = null!;

    public DateTime DateOfBirth { get; set; }

    public int? SuperpowerId { get; set; }

    public int? WeaknessId { get; set; }

    public virtual Category? Weakness { get; set; }

    public virtual ICollection<Superpower> Superpowers { get; set; } = new List<Superpower>();
}
