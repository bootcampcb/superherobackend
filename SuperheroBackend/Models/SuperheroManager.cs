﻿using Microsoft.EntityFrameworkCore;
using System.Reflection.Metadata;

namespace SuperheroBackend.Models
{
    public class SuperheroManager
    {
        private readonly SuperherosDbContext _context;

        public SuperheroManager(SuperherosDbContext context)
        {
            _context = context;
        }

        public async Task<List<Superhero>> GetAllSuperherosAsync()
        {
            //            return await _context.Superheroes.ToListAsync();
            return await _context.Superheroes
                           .Include(s => s.Superpowers)
                           .ToListAsync();

        }
        public async Task<List<Superhero>> GetLast3SuperherosAsync()
        {
            var superheroesList = new List<Superhero>();
            var superheroes = await _context.Superheroes.OrderByDescending(sh => sh.Id).ToListAsync();
            int cnt = 0;
            foreach (var superhero in superheroes)
            {
                cnt++;
                superheroesList.Add(superhero);
                if (cnt >= 3)
                {
                    break;
                }
            }
            return superheroesList;

        }
        //        public async Task<Superhero> GetSuperheroByIdAsync(int id)

        public async Task<Superhero> GetSuperheroByIdAsync(int id)
        {
            var superheroes = await _context.Superheroes
                .Include(s => s.Superpowers)
                .ThenInclude(sp => sp.Superheroes)
                .Where(s => s.Id == id)
                .ToListAsync();

            return superheroes[0];
            //            return await _context.Superheroes.FindAsync(id);
        }



        public async Task<List<Superhero>> GetSuperheroByPowerAsync(int powerId)
        {
            var superheroes = await _context.Superheroes
                .Include(s => s.Superpowers)
                .ThenInclude(sp => sp.Superheroes)
                .Where(s => s.Superpowers.Any(sp => sp.Id == powerId))
                .ToListAsync();

            return superheroes;
        }

        public async Task<bool> AddSuperheroAsync(Superhero superhero)
        {
            if (_context.Superheroes == null)
            {
                return false;
            }
            for (int i = 0; i < superhero.Superpowers.Count; i++)
            {
                _context.Entry(superhero.Superpowers.ToList()[i]).State = EntityState.Unchanged;
            }

            _context.Superheroes.Add(superhero);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UpdateSuperheroAsync(int id, Superhero superhero)
        {
            if (id != superhero.Id)
            {
                return false;
            }

            Superhero oldSuperhero = _context.Superheroes.Include(s => s.Superpowers).Where(s => s.Id == id).ToList()[0];

            _context.Entry(oldSuperhero).CurrentValues.SetValues(superhero);

            // Remove unchecked superpowers
            foreach (var superpower in oldSuperhero.Superpowers.ToList())
            {
                if (!superhero.Superpowers.Any(sp => sp.Id == superpower.Id))
                {
                    oldSuperhero.Superpowers.Remove(superpower);
                }
            }

            foreach (var superpower in superhero.Superpowers)
            {
                var  existingSuperpower= oldSuperhero.Superpowers
                    .FirstOrDefault(sp => sp.Id == superpower.Id);

                if (existingSuperpower == null)
                {
                    oldSuperhero.Superpowers.Add(superpower);
                }
                else
                {
                    _context.Entry(existingSuperpower).CurrentValues.SetValues(superpower);
                }
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SuperheroExists(id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }

            return true;
        }

        public async Task<bool> DeleteSuperheroAsync(int id)
        {
            if (_context.Superheroes == null)
            {
                return false;
            }

            var superhero = await _context.Superheroes.FindAsync(id);
            if (superhero == null)
            {
                return false;
            }

            _context.Superheroes.Remove(superhero);
            await _context.SaveChangesAsync();

            return true;
        }

        private bool SuperheroExists(int id)
        {
            return (_context.Superheroes?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }

}
