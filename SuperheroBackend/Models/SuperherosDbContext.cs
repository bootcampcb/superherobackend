﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace SuperheroBackend.Models;

public partial class SuperherosDbContext : DbContext
{
    public SuperherosDbContext()
    {
    }

    public SuperherosDbContext(DbContextOptions<SuperherosDbContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Category> Categories { get; set; }

    public virtual DbSet<Superhero> Superheroes { get; set; }

    public virtual DbSet<Superpower> Superpowers { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=SuperherosDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;Trust Server Certificate=False;Application Intent=ReadWrite;Multi Subnet Failover=False");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Superhero>(entity =>
        {
//            entity.HasIndex(e => e.WeaknessId, "IX_Superheroes_WeaknessId");

//            entity.HasOne(d => d.Weakness).WithMany(p => p.Superheroes).HasForeignKey(d => d.WeaknessId);

            entity.HasMany(d => d.Superpowers).WithMany(p => p.Superheroes)
                .UsingEntity<Dictionary<string, object>>(
                    "SuperheroSuperpower",
                    r => r.HasOne<Superpower>().WithMany().HasForeignKey("SuperpowerId"),
                    l => l.HasOne<Superhero>().WithMany().HasForeignKey("SuperheroId"),
                    j =>
                    {
                        j.HasKey("SuperheroId", "SuperpowerId");
                        j.ToTable("SuperheroSuperpower");
                        j.HasIndex(new[] { "SuperpowerId" }, "IX_SuperheroSuperpower_SuperpowerId");
                    });
        });

        modelBuilder.Entity<Superpower>(entity =>
        {
            entity.HasMany(d => d.Categories).WithMany(p => p.Superpowers)
                .UsingEntity<Dictionary<string, object>>(
                    "SuperpowerCategory",
                    r => r.HasOne<Category>().WithMany().HasForeignKey("CategoryId"),
                    l => l.HasOne<Superpower>().WithMany().HasForeignKey("SuperpowerId"),
                    j =>
                    {
                        j.HasKey("SuperpowerId", "CategoryId");
                        j.ToTable("SuperpowerCategory");
                        j.HasIndex(new[] { "CategoryId" }, "IX_SuperpowerCategory_CategoryId");
                    });
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
