﻿using System;
using System.Collections.Generic;

namespace SuperheroBackend.Models;

public partial class Superpower
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string Description { get; set; } = null!;

    public virtual ICollection<Category> Categories { get; set; } = new List<Category>();

    public virtual ICollection<Superhero> Superheroes { get; set; } = new List<Superhero>();
}
