﻿using Microsoft.EntityFrameworkCore;

namespace SuperheroBackend.Models
{
    public class SuperpowersManager
    {
        private readonly SuperherosDbContext _context;

        public SuperpowersManager(SuperherosDbContext context)
        {
            _context = context;
        }

        public async Task<List<Superpower>> GetAllSuperpowersAsync()
        {
            if (_context.Superpowers == null)
            {
                return null;
            }
            return await _context.Superpowers.ToListAsync();
        }

        public async Task<Superpower> GetSuperpowerByIdAsync(int id)
        {
            if (_context.Superpowers == null)
            {
                return null;
            }
            return await _context.Superpowers.FindAsync(id);
        }

        public async Task<bool> UpdateSuperpowerAsync(int id, Superpower superpower)
        {
            if (id != superpower.Id)
            {
                return false;
            }

            _context.Entry(superpower).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SuperpowerExists(id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }

            return true;
        }

        public async Task<Superpower> AddSuperpowerAsync(Superpower superpower)
        {
            if (_context.Superpowers == null)
            {
                return null;
            }
            _context.Superpowers.Add(superpower);
            await _context.SaveChangesAsync();

            return superpower;
        }

        public async Task<bool> DeleteSuperpowerAsync(int id)
        {
            if (_context.Superpowers == null)
            {
                return false;
            }
            var superpower = await _context.Superpowers.FindAsync(id);
            if (superpower == null)
            {
                return false;
            }

            _context.Superpowers.Remove(superpower);
            await _context.SaveChangesAsync();

            return true;
        }

        private bool SuperpowerExists(int id)
        {
            return (_context.Superpowers?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
